import os

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from .models import Base

current_dir = os.path.dirname(__file__)
engine = create_engine('sqlite:///' + os.path.join(current_dir, 'ttt.sqlite3'))

Base.metadata.create_all(engine)

session = sessionmaker(bind=engine)()
