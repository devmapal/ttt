from datetime import datetime, timedelta

from sqlalchemy import (
    Boolean,
    Column,
    DateTime,
    ForeignKey,
    Integer,
    String,
    func,
)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy.orm.session import object_session


class Base(object):
    id = Column(Integer, primary_key=True)
    Column('create_date', DateTime, server_default=func.now()),
    last_modified = Column(
        'last_modified',
        DateTime,
        server_default=func.now(),
        onupdate=datetime.utcnow,
    )


Base = declarative_base(cls=Base)


class Sprint(Base):
    __tablename__ = 'sprints'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    issues = relationship('Issue', back_populates='sprint')

    committed_points = Column(Integer)
    productive_hours = Column(Integer)

    def __repr__(self):
        return self.name


class Issue(Base):
    __tablename__ = 'issues'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    isCodeReview = Column(Boolean, default=False)
    isFirefighting = Column(Boolean, default=False)
    isFunctionalReview = Column(Boolean, default=False)
    storyPoints = Column(Integer, default=0)

    sprint_id = Column(Integer, ForeignKey('sprints.id'))
    sprint = relationship('Sprint', back_populates='issues')

    timespans = relationship('Timespan', back_populates='issue')

    def get_time_spent(self):
        time_spent = timedelta()

        now = datetime.utcnow()
        for timespan in self.timespans:
            if timespan.end:
                time_spent += timespan.end - timespan.start
            else:
                time_spent += now - timespan.start

        return time_spent

    def is_counting(self):
        session = object_session(self)
        timespan = session.query(Timespan).filter_by(issue_id=self.id).filter(
            Timespan.end == None).one_or_none()
        return timespan is not None

    def start_timer(self):
        # Probably need to use with_for_update (read -> update -> write)
        session = object_session(self)
        timespan = session.query(Timespan).filter_by(issue_id=self.id).filter(
            Timespan.end == None).one_or_none()

        if timespan is None:
            self.timespans.append(Timespan())
            session.commit()

    def stop_timer(self):
        # Probably need to use with_for_update (read -> update -> write)
        session = object_session(self)
        timespan = session.query(Timespan).filter_by(issue_id=self.id).filter(
            Timespan.end == None).one_or_none()

        if timespan is not None:
            timespan.end = datetime.utcnow()
            session.commit()

    def __repr__(self):
        result = self.name

        if self.sprint and self.sprint.productive_hours:
            points_per_hour =(
                self.sprint.committed_points / self.sprint.productive_hours)
            hours_spent = self.get_time_spent().total_seconds() / 3600
            points_spent = hours_spent * points_per_hour

            result += ' ({:.2f}/{})'.format(points_spent, self.storyPoints)

        return result


class Timespan(Base):
    __tablename__ = 'timespans'

    id = Column(Integer, primary_key=True)
    start = Column(DateTime, default=func.now())
    end = Column(DateTime)

    issue_id = Column(Integer, ForeignKey('issues.id'))
    issue = relationship('Issue', back_populates='timespans')
