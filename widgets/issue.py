# -*- coding: utf-8 -*-

from PyQt5.QtWidgets import (
    QCheckBox,
    QFormLayout,
    QLabel,
    QLineEdit,
    QPushButton,
    QVBoxLayout,
    QWidget,
    QHBoxLayout,
)
from PyQt5.QtCore import QCoreApplication, pyqtSignal
from PyQt5.Qt import QSizePolicy
from PyQt5.QtCore import Qt, QTimer

from db.models import Timespan
from db.session import session
from state_manager import stateManager
from widgets.edit_issue_dialog import EditIssueDialog


class Issue(QWidget):

    started = pyqtSignal()

    def __init__(self, issueModelInstance, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._modelInstance = issueModelInstance
        stateManager.addWidget(self._modelInstance.id, self)

        self.setupUi()

    def setupUi(self):
        _translate = QCoreApplication.translate

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")

        # Issue name
        self.issueLabel = QLabel()
        self.issueLabel.setObjectName("issueLabel")
        self.horizontalLayout.addWidget(self.issueLabel)

        # Type label
        self.typeLabel = QLabel()
        self.typeLabel.setObjectName("typeLabel")
        self.horizontalLayout.addWidget(self.typeLabel)

        self.timeSpent = QLabel()
        self.timeSpent.setObjectName("timeSpent")
        self.timeSpent.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Fixed)
        self.horizontalLayout.addWidget(self.timeSpent)

        self.editIssueButton = QPushButton()
        self.editIssueButton.setObjectName("editIssueButton")
        self.editIssueButton.setText(
            QCoreApplication.translate("Issue", "Edit issue"))
        self.editIssueButton.clicked.connect(self._editIssueButtonClickedHandler)
        self.horizontalLayout.addWidget(self.editIssueButton)

        self.toggleButton = QPushButton()
        self.toggleButton.setCheckable(True)
        self.toggleButton.setObjectName("toggleButton")
        self.toggleButton.toggled.connect(self._toggleButtonToggledHandler)
        self.toggleButton.setText(_translate("Issue", "Start"))

        self.horizontalLayout.addWidget(self.toggleButton)

        self.setLayout(self.horizontalLayout)
        self._updateUi()

    def _editIssueButtonClickedHandler(self):
        (name,
         storyPoints,
         isCodeReview,
         isFirefighting,
         isFunctionalReview,
         result) = EditIssueDialog.getIssueData(
             name=self._modelInstance.name,
             isCodeReview=self._modelInstance.isCodeReview,
             isFirefighting=self._modelInstance.isFirefighting,
             isFunctionalReview=self._modelInstance.isFunctionalReview,
             storyPoints=self._modelInstance.storyPoints,
             parent=self)

        if not result:
            return

        self._modelInstance.name = name
        self._modelInstance.storyPoints = storyPoints
        self._modelInstance.isCodeReview = isCodeReview
        self._modelInstance.isFirefighting = isFirefighting
        self._modelInstance.isFunctionalReview = isFunctionalReview
        session.add(self._modelInstance)
        session.commit()

        self._updateUi()

    def _updateUi(self):
        _translate = QCoreApplication.translate

        self.issueLabel.setText(str(self._modelInstance))

        if self._modelInstance.isCodeReview:
            self.typeLabel.setText(
                _translate("Issue", "<i>is code review</i>"))
        elif self._modelInstance.isFirefighting:
            self.typeLabel.setText(
                _translate("Issue", "<i>is firefighting</i>"))
        elif self._modelInstance.isFunctionalReview:
            self.typeLabel.setText(
                _translate("Issue", "<i>is functionalReview</i>"))
        else:
            self.typeLabel.setText("")

        timeSpent = str(self._modelInstance.get_time_spent()).split('.')[0]
        self.timeSpent.setText(timeSpent)

    def _updateTimeSpent(self):
        self._updateUi()
        if self._modelInstance.is_counting() is True:
            QTimer.singleShot(1000, self._updateTimeSpent)

    def _toggleButtonToggledHandler(self, checked):
        if checked is True:
            stateManager.start(self._modelInstance.id)
        else:
            stateManager.stop()

    def start(self):
        _translate = QCoreApplication.translate
        self._updateTimeSpent()
        self.toggleButton.setText(_translate("Issue", "Stop"))
        old_state = self.toggleButton.blockSignals(True)
        self.toggleButton.setChecked(True)
        self.toggleButton.blockSignals(old_state)

    def stop(self):
        _translate = QCoreApplication.translate
        self._updateTimeSpent()
        self.toggleButton.setText(_translate("Issue", "Start"))
        old_state = self.toggleButton.blockSignals(True)
        self.toggleButton.setChecked(False)
        self.toggleButton.blockSignals(old_state)
