from PyQt5.QtCore import QCoreApplication, Qt, QDateTime
from PyQt5.QtWidgets import (
    QCheckBox,
    QDateTimeEdit,
    QDialog,
    QDialogButtonBox,
    QFormLayout,
    QLabel,
    QLineEdit,
    QSpinBox,
    QVBoxLayout,
)


class EditSprintDialog(QDialog):
    def __init__(self,
                 name="",
                 committedPoints=0,
                 productiveHours=0,
                 *args,
                 **kwargs):
        super().__init__(*args, **kwargs)

        layout = QVBoxLayout(self)
        self.sprintFormLayout = QFormLayout()
        self.sprintFormLayout.setObjectName("sprintFormLayout")

        # Sprint edit
        self.sprintLabel = QLabel()
        self.sprintLabel.setObjectName("sprintLabel")
        self.sprintFormLayout.setWidget(
            0, QFormLayout.LabelRole, self.sprintLabel)

        self.sprintLineEdit = QLineEdit()
        self.sprintLineEdit.setText(name)
        self.sprintLineEdit.setObjectName("sprintLineEdit")
        self.sprintFormLayout.setWidget(
            0, QFormLayout.FieldRole, self.sprintLineEdit)

        # committed points
        self.committedPointsLabel = QLabel()
        self.committedPointsLabel.setObjectName("committedPointsLabel")
        self.sprintFormLayout.setWidget(
            1, QFormLayout.LabelRole, self.committedPointsLabel)

        self.committedPointsSpinBox = QSpinBox()
        self.committedPointsSpinBox.setValue(committedPoints)
        self.committedPointsSpinBox.setMinimum(0)
        self.committedPointsSpinBox.setObjectName("committedPointsSpinBox")
        self.sprintFormLayout.setWidget(
            1, QFormLayout.FieldRole, self.committedPointsSpinBox)

        # productive hours
        self.productiveHoursLabel = QLabel()
        self.productiveHoursLabel.setObjectName("productiveHoursLabel")
        self.sprintFormLayout.setWidget(
            2, QFormLayout.LabelRole, self.productiveHoursLabel)

        self.productiveHoursSpinBox = QSpinBox()
        self.productiveHoursSpinBox.setValue(productiveHours)
        self.productiveHoursSpinBox.setMinimum(0)
        self.productiveHoursSpinBox.setObjectName("productiveHoursSpinBox")
        self.sprintFormLayout.setWidget(
            2, QFormLayout.FieldRole, self.productiveHoursSpinBox)

        self.retranslateUi()

        layout.addLayout(self.sprintFormLayout)

        # OK and Cancel buttons
        buttons = QDialogButtonBox(
            QDialogButtonBox.Ok | QDialogButtonBox.Cancel,
            Qt.Horizontal, self)
        buttons.accepted.connect(self.accept)
        buttons.rejected.connect(self.reject)
        layout.addWidget(buttons)

    def retranslateUi(self):
        _translate = QCoreApplication.translate
        self.setWindowTitle(_translate("Sprint", "Edit sprint"))
        self.sprintLabel.setText(_translate("Sprint", "Sprint"))
        self.committedPointsLabel.setText(_translate("Sprint", "Committed points"))
        self.productiveHoursLabel.setText(_translate("Sprint", "Productive hours"))

    # get current date and time from the dialog
    def sprintName(self):
        return self.sprintLineEdit.text()

    def committedPoints(self):
        return self.committedPointsSpinBox.value()

    def productiveHours(self):
        return self.productiveHoursSpinBox.value()

    @staticmethod
    def getSprintData(name="",
                      committedPoints=0,
                      productiveHours=0,
                      parent=None):
        dialog = EditSprintDialog(
            name=name,
            committedPoints=committedPoints,
            productiveHours=productiveHours,
            parent=parent)
        result = dialog.exec_()
        return (
            dialog.sprintName(),
            dialog.committedPoints(),
            dialog.productiveHours(),
            result == QDialog.Accepted,
        )
