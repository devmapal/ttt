# -*- coding: utf-8 -*-

from PyQt5.QtWidgets import QPushButton, QVBoxLayout, QScrollArea, QWidget
from PyQt5.QtCore import QCoreApplication, pyqtSignal
from PyQt5.Qt import QSizePolicy
from PyQt5.QtCore import Qt

from db.models import Issue as IssueModel
from db.session import session
from widgets.edit_issue_dialog import EditIssueDialog
from widgets.issue import Issue as IssueWidget


class Sprint(QWidget):
    def __init__(self, modelInstance, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._modelInstance = modelInstance
        self._issueWidgets = []

        self.setupUi()

        for issue in session.query(IssueModel).filter_by(sprint=modelInstance):
            self.addIssue(issue)

    def setupUi(self):
        self.layout = QVBoxLayout(self)

        self.scrollArea = QScrollArea()
        self.scrollArea.setWidgetResizable(True)
        self.layout.addWidget(self.scrollArea)

        contentWidget = QWidget()
        self.verticalLayout = QVBoxLayout()
        self.verticalLayout.setAlignment(Qt.AlignTop)
        self.verticalLayout.setObjectName("verticalLayout")
        contentWidget.setLayout(self.verticalLayout)

        self.scrollArea.setWidget(contentWidget)

        self.addIssueButton = QPushButton()
        self.addIssueButton.setObjectName("addIssueButton")
        self.addIssueButton.setText(
            QCoreApplication.translate("Sprint", "Add Issue"))
        self.addIssueButton.clicked.connect(self.addIssueButtonClickedHandler)
        self.layout.addWidget(self.addIssueButton)

        self.setLayout(self.layout)

    def addIssueButtonClickedHandler(self):
        (name,
         storyPoints,
         isCodeReview,
         isFirefighting,
         isFunctionalReview,
         result) = EditIssueDialog.getIssueData(parent=self)

        if not result:
            return

        issueModelInstance = IssueModel(
            name=name,
            isCodeReview=isCodeReview,
            isFirefighting=isFirefighting,
            isFunctionalReview=isFunctionalReview,
            storyPoints=storyPoints,
            sprint=self._modelInstance,
        )
        session.add(issueModelInstance)
        session.commit()

        self.addIssue(issueModelInstance)

    def addIssue(self, issueModelInstance):
        issueWidget = IssueWidget(issueModelInstance)
        self._issueWidgets.append(issueWidget)
        self.verticalLayout.insertWidget(0, issueWidget)
