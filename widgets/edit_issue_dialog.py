from PyQt5.QtCore import QCoreApplication, Qt, QDateTime
from PyQt5.QtWidgets import (
    QCheckBox,
    QDateTimeEdit,
    QDialog,
    QDialogButtonBox,
    QFormLayout,
    QLabel,
    QLineEdit,
    QDoubleSpinBox,
    QVBoxLayout,
)


class EditIssueDialog(QDialog):
    def __init__(self,
                 name="",
                 isCodeReview=False,
                 isFirefighting=False,
                 isFunctionalReview=False,
                 storyPoints=0,
                 *args,
                 **kwargs):
        super().__init__(*args, **kwargs)

        layout = QVBoxLayout(self)
        self.issueFormLayout = QFormLayout()
        self.issueFormLayout.setObjectName("issueFormLayout")

        # Issue edit
        self.issueLabel = QLabel()
        self.issueLabel.setObjectName("issueLabel")
        self.issueFormLayout.setWidget(
            0, QFormLayout.LabelRole, self.issueLabel)

        self.issueLineEdit = QLineEdit()
        self.issueLineEdit.setText(name)
        self.issueLineEdit.setObjectName("issueLineEdit")
        self.issueFormLayout.setWidget(
            0, QFormLayout.FieldRole, self.issueLineEdit)

        # story points
        self.storyPointsLabel = QLabel()
        self.storyPointsLabel.setObjectName("storyPointsLabel")
        self.issueFormLayout.setWidget(
            1, QFormLayout.LabelRole, self.storyPointsLabel)

        self.storyPointsSpinBox = QDoubleSpinBox()
        self.storyPointsSpinBox.setValue(storyPoints)
        self.storyPointsSpinBox.setMinimum(0)
        self.storyPointsSpinBox.setDecimals(1)
        self.storyPointsSpinBox.setObjectName("issueLineEdit")
        self.issueFormLayout.setWidget(
            1, QFormLayout.FieldRole, self.storyPointsSpinBox)

        # Code review checkbox
        self.codeReviewLabel = QLabel()
        self.codeReviewLabel.setObjectName("codeReviewLabel")
        self.issueFormLayout.setWidget(
            2, QFormLayout.LabelRole, self.codeReviewLabel)

        self.codeReviewCheckBox = QCheckBox()
        self.codeReviewCheckBox.setChecked(isCodeReview)
        self.codeReviewCheckBox.setObjectName("codeReviewCheckBox")
        self.issueFormLayout.setWidget(
            2, QFormLayout.FieldRole, self.codeReviewCheckBox)

        # Firefighting checkbox
        self.firefightingLabel = QLabel()
        self.firefightingLabel.setObjectName("firefightingLabel")
        self.issueFormLayout.setWidget(
            3, QFormLayout.LabelRole, self.firefightingLabel)

        self.firefightingCheckBox = QCheckBox()
        self.firefightingCheckBox.setChecked(isFirefighting)
        self.firefightingCheckBox.setObjectName("firefightingCheckBox")
        self.issueFormLayout.setWidget(
            3, QFormLayout.FieldRole, self.firefightingCheckBox)

        # Functional review checkbox
        self.functionalReviewLabel = QLabel()
        self.functionalReviewLabel.setObjectName("functionalReviewLabel")
        self.issueFormLayout.setWidget(
            4, QFormLayout.LabelRole, self.functionalReviewLabel)

        self.functionalReviewCheckBox = QCheckBox()
        self.functionalReviewCheckBox.setChecked(isFunctionalReview)
        self.functionalReviewCheckBox.setObjectName("functionalReviewCheckBox")
        self.issueFormLayout.setWidget(
            4, QFormLayout.FieldRole, self.functionalReviewCheckBox)

        self.retranslateUi()

        layout.addLayout(self.issueFormLayout)

        # OK and Cancel buttons
        buttons = QDialogButtonBox(
            QDialogButtonBox.Ok | QDialogButtonBox.Cancel,
            Qt.Horizontal, self)
        buttons.accepted.connect(self.accept)
        buttons.rejected.connect(self.reject)
        layout.addWidget(buttons)

    def retranslateUi(self):
        _translate = QCoreApplication.translate
        self.setWindowTitle(_translate("Issue", "Edit issue"))
        self.issueLabel.setText(_translate("Issue", "Issue"))
        self.storyPointsLabel.setText(_translate("Issue", "Story points"))
        self.codeReviewLabel.setText(_translate("Issue", "Code review"))
        self.firefightingLabel.setText(_translate("Issue", "Firefighting"))
        self.functionalReviewLabel.setText(_translate("Issue", "Functional review"))

    # get current date and time from the dialog
    def issueName(self):
        return self.issueLineEdit.text()

    def storyPoints(self):
        return self.storyPointsSpinBox.value()

    def isCodeReview(self):
        return self.codeReviewCheckBox.isChecked()

    def isFirefighting(self):
        return self.firefightingCheckBox.isChecked()

    def isFunctionalReview(self):
        return self.functionalReviewCheckBox.isChecked()

    @staticmethod
    def getIssueData(name="",
                     isCodeReview=False,
                     isFirefighting=False,
                     isFunctionalReview=False,
                     storyPoints=0,
                     parent=None):
        dialog = EditIssueDialog(
            name=name,
            isCodeReview=isCodeReview,
            isFirefighting=isFirefighting,
            isFunctionalReview=isFunctionalReview,
            storyPoints=storyPoints,
            parent=parent)
        result = dialog.exec_()
        return (
            dialog.issueName(),
            dialog.storyPoints(),
            dialog.isCodeReview(),
            dialog.isFirefighting(),
            dialog.isFunctionalReview(),
            result == QDialog.Accepted,
        )
