# -*- coding: utf-8 -*-

from PyQt5.QtWidgets import QTabWidget, QWidget

from db.models import Sprint as SprintModel
from db.session import session
from widgets.sprint import Sprint as SprintWidget
from widgets.edit_sprint_dialog import EditSprintDialog


class Sprints(QTabWidget):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._sprintWidgets = []

        self.setupUi()

    def setupUi(self):
        self.addSprintWidget = QWidget()
        self.addTab(self.addSprintWidget, "+")

        for sprint in session.query(SprintModel):
            self.addSprint(SprintWidget(sprint), sprint.name)

        self.tabBarClicked.connect(self.handleTabBarClicked)

    def handleTabBarClicked(self, index):
        if self.widget(index) == self.addSprintWidget:
            (name,
             committedPoints,
             productiveHours,
             ok) = EditSprintDialog.getSprintData(parent=self)
            if ok is False:
                return

            sprintModelInstance = SprintModel(
                name=name,
                committed_points=committedPoints,
                productive_hours=productiveHours,
            )
            session.add(sprintModelInstance)
            session.commit()

            self.addSprint(SprintWidget(sprintModelInstance), name)

    def addSprint(self, sprintWidget, name):
        self._sprintWidgets.append(sprintWidget)
        self.insertTab(0, sprintWidget, name)
        self.setCurrentIndex(0)
