from PyQt5.QtWidgets import QMainWindow

from db.models import Issue
from db.session import session
from widgets.sprints import Sprints


class MainWindow(QMainWindow):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setCentralWidget(Sprints())
        self.setGeometry(300, 300, 350, 300)
        self.setWindowTitle('ttt')

    def closeEvent(self, event):
        # TODO: Write method for this
        for issue in session.query(Issue):
            issue.stop_timer()
        event.accept()
