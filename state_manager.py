from PyQt5.QtCore import QTimer

from db.models import Issue
from db.session import session
from user_metrics import UserMetrics


class StateManager(object):
    PAUSE_AFTER_MS = 60000

    def __init__(self):
        self._running_issue_id = None
        self._widget_by_issue_id = {}
        try:
            self._user_metrics = UserMetrics.create()
        except NotImplementedError:
            self._user_metrics = None

    def _running_issue(self):
        return session.query(Issue).filter_by(id=self._running_issue_id).one()

    def _running_widget(self):
        return self._widget_by_issue_id.get(self._running_issue_id)

    def _handle_inactivity(self):
        if self._running_issue_id is None:
            return

        idle_since = self._user_metrics.idle_since()
        if idle_since > self.PAUSE_AFTER_MS:
            self.pause()
            QTimer.singleShot(100, self._handle_activity)
        else:
            QTimer.singleShot(
                self.PAUSE_AFTER_MS - idle_since,
                self._handle_inactivity,
            )

    def _handle_activity(self):
        if self._running_issue_id is None:
            return

        idle_since = self._user_metrics.idle_since()
        if idle_since < self.PAUSE_AFTER_MS:
            self.start(self._running_issue_id)
        else:
            QTimer.singleShot(100, self._handle_activity)

    def start(self, issue_id):
        self.stop()
        self._running_issue_id = issue_id
        self._running_issue().start_timer()
        self._running_widget().start()

        if self._user_metrics:
            self._handle_inactivity()

    def pause(self):
        if self._running_issue_id is not None:
            self._running_issue().stop_timer()
            self._running_widget().stop()

    def stop(self):
        self.pause()
        self._running_issue_id = None

    def addWidget(self, issue_id, widget):
        self._widget_by_issue_id[issue_id] = widget


stateManager = StateManager()
