import ctypes
import platform


class UserMetrics(object):
    @staticmethod
    def create():
        if platform.system() == 'Linux':
            return LinuxUserMetrics()
        else:
            raise NotImplementedError()

    def idle_since(self):
        raise NotImplementedError()


class LinuxUserMetrics(UserMetrics):
    def __init__(self):
        class XScreenSaverInfo(ctypes.Structure):
            """ typedef struct { ... } XScreenSaverInfo; """
            _fields_ = [
                ('window', ctypes.c_ulong),         # screen saver window
                ('state', ctypes.c_int),            # off,on,disabled
                ('kind', ctypes.c_int),             # blanked,internal,external
                ('since', ctypes.c_ulong),          # milliseconds
                ('idle', ctypes.c_ulong),           # milliseconds
                ('event_mask', ctypes.c_ulong),     # events
            ]

        xlib = ctypes.cdll.LoadLibrary('libX11.so')
        self.dpy = xlib.XOpenDisplay(None)
        self.root = xlib.XDefaultRootWindow(self.dpy)
        self.xss = ctypes.cdll.LoadLibrary('libXss.so')
        self.xss.XScreenSaverAllocInfo.restype = ctypes.POINTER(
            XScreenSaverInfo)
        self.xss_info = self.xss.XScreenSaverAllocInfo()

    def idle_since(self):
        self.xss.XScreenSaverQueryInfo(self.dpy, self.root, self.xss_info)
        return self.xss_info.contents.idle
